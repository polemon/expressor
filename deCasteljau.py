# module expressor
# expressor.py is a compander
#  but "compander" is a shitty name

# deg 1
def linear(a, b):
    return lambda t: a * (1.0 - t) + b * t

# deg 2
def quadratic(a, b, c):
    return lambda t: linear( linear(a,b)(t),
                             linear(b,c)(t) )(t)

# deg 3
def cubic(a, b, c, d):
    return lambda t: linear( quadratic(a,b,c)(t),
                             quadratic(b,c,d)(t) )(t)

# deg 4
def quartic(a, b, c, d, e):
    return lambda t: linear( cubic(a,b,c,d)(t),
                             cubic(b,c,d,e)(t) )(t)

# deg 5
def quintic(a, b, c, d, e, f):
    return lambda t: linear( quartic(a,b,c,d,e)(t),
                             quartic(b,c,d,e,f)(t) )(t)

# deg 6
def sextic(a, b, c, d, e, f, g):
    return lambda t: linear( quintic(a,b,c,d,e,f)(t),
                             quintic(b,c,d,e,f,g)(t) )(t)

# deg 7
def septic(a, b, c, d, e, f, g, h):
    return lambda t: linear( sextic(a,b,c,d,e,f,g)(t),
                             sextic(b,c,d,e,f,g,h)(t) )(t)

# deg 8
def octic(a, b, c, d, e, f, g, h, i):
    return lambda t: linear( septic(a,b,c,d,e,f,g,h)(t),
                             septic(b,c,d,e,f,g,h,i)(t) )(t)

# deg 9
def nonic(a, b, c, d, e, f, g, h, i, j):
    return lambda t: linear( octic(a,b,c,d,e,f,g,h,i)(t),
                             octic(b,c,d,e,f,g,h,i,j)(t) )(t)

# deg 10
def decic(a, b, c, d, e, f, g, h, i, j, k):
    return lambda t: linear( nonic(a,b,c,d,e,f,g,h,i,j)(t),
                             nonic(b,c,d,e,f,g,h,i,j,k)(t) )(t)
