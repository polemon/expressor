import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk, GLib, cairo
import math
import cairo

class mainWin(Gtk.Window):
    #FIXME This must later go into the controller!
    def _paint(self, widget, cr):
        cr.set_source_rgba(0.25, 0.25, 0.25, 0.25)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.paint()
        
    
    def __init__(self, **kw):
        super(mainWin, self).__init__( default_width = 800,
                                       default_height = 900,
                                       title = "Expressor",
                                       **kw )
        self.set_border_width(15)
        self._css = b"""
            frame > border {
                border-color: #888;
                border-radius: 10pt;
            }

            frame > label {
                padding-left: 3pt;
                padding-right: 3pt;
            }

            #coeff_frame {
                padding-top: 10pt;
            }

            #coeff_container {
                padding: 20pt;
            }

            scale {
                min-height: 120pt;
                padding: 5pt;
            }

            scale > value {
                color: #aaa;
            }
        """

        self._mag_slider_name = [ "primary",
                                  "secondary",
                                  "tertiary",
                                  "quaternary",
                                  "quinary",
                                  "senary",
                                  "septenary",
                                  "octonary",
                                  "nonary",
                                  "denary" ]

        self._style_provider = Gtk.CssProvider()
        self._style_provider.load_from_data(self._css)

        # Gtk3 style headerbar
        self._headerbar = Gtk.HeaderBar()
        self._headerbar.set_name("headerbar")
        self._headerbar.set_show_close_button(True)
        self._headerbar.set_title("Expressor")
        self._headerbar.props.title = "Expressor"
        self.set_titlebar(self._headerbar)


        self._outter_vbox = Gtk.VBox()
        self._outter_vbox.set_name("outter_vbox")
        self.add(self._outter_vbox)

        self.da = Gtk.DrawingArea()
        self.da.connect("draw", self._paint)
        #self.da.set_size_request(10, 100)
        self._outter_vbox.pack_start(self.da, True, True, 0)

        # frame encompassing the coefficient sliders
        self._slframe = Gtk.Frame()
        self._slframe.set_name("coeff_frame")
        self._slframe.set_label("Coefficients")
        self._slframe.set_label_align(0.01, 0.5)
        self._slframe.set_shadow_type(Gtk.ShadowType.OUT)
        self._outter_vbox.pack_start(self._slframe, False, True, 0)

        # horizontal alignment field for coefficient sliders
        self._hbox = Gtk.HBox()
        self._hbox.set_name("coeff_container")
        self._hbox.set_homogeneous(True)
        # this can be set at .pack_start()
        #self._hbox.set_spacing(20)
        self._slframe.add(self._hbox)

        # sliders used for coefficients
        self.mag_slider = []

        # container for label + slider because EVERYSHIT needs a box
        self._slider_stack = []

        for i in range(len(self._mag_slider_name)):
            self.mag_slider.append( Gtk.Scale( orientation = Gtk.Orientation.VERTICAL,
                                               adjustment = Gtk.Adjustment( lower = 0.000, 
                                                                            upper = 1.010,
                                                                            step_increment = 0.001,
                                                                            page_increment = 0.01,
                                                                            page_size = 0.01 ) ) )
            self.mag_slider[i].connect("format_value", lambda self, x: "%.3f" % (x))
            self.mag_slider[i].set_name(self._mag_slider_name[i])
            self.mag_slider[i].set_inverted(True)
            self.mag_slider[i].set_round_digits(4)
            #self.mag_slider[i].set_size_request(-1, 100)

            self._slider_stack.append( Gtk.VBox() )
            self._slider_stack[i].pack_start(Gtk.Label(self._mag_slider_name[i]), True, True, 3)
            self._slider_stack[i].pack_start(self.mag_slider[i], False, True, 3)

            self._hbox.pack_start(self._slider_stack[i], True, True, 3)
            


        Gtk.StyleContext.add_provider_for_screen( Gdk.Screen.get_default(),
                                                  self._style_provider,
                                                  Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION )
