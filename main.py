#!/usr/bin/env python3

import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk, Gdk, GLib
from view import *

import numpy as np
import pprint as pp
import matplotlib.pyplot as plt
import deCasteljau as dC


class epControl(object):
    def __init__(self, view):
        self._view = view
        self._view.connect("destroy", Gtk.main_quit)
        self._view.show_all()


def main():
    epControl(mainWin())
    Gtk.main()
    pass


if __name__ == "__main__":
    main()
