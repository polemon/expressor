#! /usr/bin/env python3

import numpy as np
import pprint as pp
import matplotlib.pyplot as plt
import deCasteljau as dC

def main():
    # min = 0.0, max = 1.0
    mag_e =  0.8
    mag_1 =  0.5
    mag_2 =  0.5
    mag_3 =  0.5
    mag_4 =  0.5
    mag_5 =  0.0
    mag_6 =  0.0
    mag_7 =  0.0
    mag_8 =  0.0
    mag_9 =  0.0
    mag_10 = 1.0

    inp    = np.arange(0.0, 1.0, 0.01)
    out_e  = [ np.exp(mag_e * (1 - 1/x)) for x in inp ]
    out_1  = [ dC.linear(    0.0, mag_1)(x) for x in inp ]
    out_2  = [ dC.quadratic( 0.0, mag_1, mag_2)(x) for x in inp ]
    out_3  = [ dC.cubic(     0.0, mag_1, mag_2, mag_3)(x) for x in inp ]
    out_4  = [ dC.quartic(   0.0, mag_1, mag_2, mag_3, mag_4)(x) for x in inp ]
    out_5  = [ dC.quintic(   0.0, mag_1, mag_2, mag_3, mag_4, mag_5)(x) for x in inp ]
    out_6  = [ dC.sextic(    0.0, mag_1, mag_2, mag_3, mag_4, mag_5, mag_6)(x) for x in inp ]
    out_7  = [ dC.septic(    0.0, mag_1, mag_2, mag_3, mag_4, mag_5, mag_6, mag_7)(x) for x in inp ]
    out_8  = [ dC.octic(     0.0, mag_1, mag_2, mag_3, mag_4, mag_5, mag_6, mag_7, mag_8)(x) for x in inp ]
    out_9  = [ dC.nonic(     0.0, mag_1, mag_2, mag_3, mag_4, mag_5, mag_6, mag_7, mag_8, mag_9)(x) for x in inp ]
    out_10 = [ dC.decic(     0.0, mag_1, mag_2, mag_3, mag_4, mag_5, mag_6, mag_7, mag_8, mag_9, mag_10)(x) for x in inp ]

    #pp.pprint(inp)
    #pp.pprint(outp)

    plt.figure(figsize=(10, 8))
    ax = plt.subplot(111)

    plt.plot(inp, label="input")
    plt.plot(out_e, label="e^(%0.1f * (1 - 1/x))" % mag_e)
    plt.plot(out_1, label="linear %0.1f" % mag_1)
    plt.plot(out_2, label="quadratic %0.1f" % mag_2)
    plt.plot(out_3, label="cubic %0.1f" % mag_3)
    plt.plot(out_4, label="quartic %0.1f" % mag_4)
    plt.plot(out_5, label="quintic %0.1f" % mag_5)
    plt.plot(out_6, label="sextic %0.1f" % mag_6)
    plt.plot(out_7, label="septic %0.1f" % mag_7)
    plt.plot(out_8, label="octic %0.1f" % mag_8)
    plt.plot(out_9, label="nonic %0.1f" % mag_9)
    plt.plot(out_10, label="decic %0.1f" % mag_10)

    box = ax.get_position()
    ax.set_position([ box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9 ])
    ax.legend(loc="upper center", bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    plt.show()

if __name__ == "__main__":
    main()
